# PDM lab retake - final evaluation

1. Please follow the [setup instructions](https://gitlab.com/pdm-bigdata/setup) to install and run PySpark and Jupyter Notebook.

2. Then [download this repository](https://gitlab.com/pdm-bigdata/lab-spark-retake/-/archive/master/lab-spark-retake-master.zip) and unzip it.

3. From the Jupyter Notebook in your browser, open the `spark_lab.ipynb` notebook, run it and answer the questions.

4. Save the `spark_lab.ipynb` notebook and send it by e-mail to your teacher.
